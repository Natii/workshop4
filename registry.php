<?php
    require("connection.php");
    require("user.php");
    $conn = Connect();
    if ($_POST) {
        $username = $_REQUEST['username'];
        $password = $_REQUEST['password'];
        $name = $_REQUEST['name'];
        $lastname = $_REQUEST['lastname'];

        $user = new User($username, $password, $name, $lastname);
        if (mysqli_query($conn, $user->InsertUser())) {
            header("Location: indexRegistry.php");
            $_SESSION['message'] = "Customer successfully registered";
            $_SESSION['message_type'] = "success";
           
        } else {
            $_SESSION['message'] = "Failed to register client";
            $_SESSION['message_type'] = "error";
        }
    }
?>