<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Workshop #4: PHP Sessions</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
    crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3>Form Customer Registration</h3>
    <form method="POST" name="form" action="registry.php">
      <div class="form-group">
        <label class="sr-only" for="">Username</label>
        <input type="text" class="form-control" id="" name="username" placeholder="Your username">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Password</label>
        <input type="password" class="form-control" id="" name="password" placeholder="Your password">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Name</label>
        <input type="text" class="form-control" id="" name="name" placeholder="Your name">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Lastname</label>
        <input type="text" class="form-control" id="" name="lastname" placeholder="Your lastname">
      </div>

      <button type="submit" class="btn btn-primary">Register</button>
      
      <a href="dashboard.php"><input type="button" value="Back" class="btn btn-primary"></a>

    </form>
</div>
</body>
</html>