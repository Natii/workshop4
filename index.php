<?php
      session_start();
      if ($_SESSION && $_SESSION['user']){
        //user already logged in
        header('Location: dashboard.php');
      }
    
      $message = "";
      if(!empty($_REQUEST['status'])) {
        switch($_REQUEST['status']) {
          case 'login':
            $message = 'User does not exists';
          break;
          case 'error':
            $message = 'There was a problem inserting the user';
          break;
        }
      }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Workshop #4: PHP Sessions</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
    crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="alert alert-primary" role="alert">
      <?php echo $message; ?>
    </div>
    <h3>User Login</h3>
    <form method="POST" name="form" action="login.php">
      <div class="form-group">
        <label class="sr-only" for="">Username</label>
        <input type="text" class="form-control" id="" name="username" placeholder="Your username">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Password</label>
        <input type="password" class="form-control" id="" name="password" placeholder="Your password">
      </div>

      <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>
</body>
</html>